#include "proc.h"
#include <stdio.h>
#include <stdlib.h>

void read_file(char *file_name) {

	FILE *fd;
	long ticks;


	if((fd = fopen(file_name, "r")) == NULL){
		printf("Error while opening file.\n");
		exit(-1);
	}

	while (fscanf(fd, "%ld", &ticks) != EOF){
					//fscanf(fd, "%d", &priority); fitxategian bi balioak jartzen baditugu, zikloak eta lehentasuna espazio batez bananduak lerro berean, honen bidez irakurriko genuke.
					int priority=(rand() % (99 + 1 - 0)) + 0; //0 eta 100 arteko zorizko lehentasunak sortuko ditu.
					create_new_process(ticks, priority);
					printf("Ticks %ld\n", ticks);
	}
	fclose(fd);
}
