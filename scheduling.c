#include "list.h"
#include "scheduling.h"
#include "proc.h"
#include "globals.h"
#include "computing_engine.h"
#include <stdio.h>
#include <stdlib.h>

list_t ready_queue;
list_t prior_list[100];


struct pcb *scheduler(){

    struct pcb *proc;

    switch(conf.scheduling_policy){
        case FIFO:
            list_head(&ready_queue,&proc);
            //break;
            return(proc);
        case PRIORITY:
          //printf("Scheduler funtzioan, priority kasuaren azterketa...\n");
          for(int i=99; i>-1; i--){
            if(!list_empty(&prior_list[i])){
              list_head(&prior_list[i], &proc); //Lehentasunen politiken barruan, bakoitzaren prest ilara atzitzeko FIFO politika aplikatuko dugu.
              //printf("Scheduler funtzioan, %d lehentasuneko %d prozesua (%d lehent %ld tick)\n", i, proc->pid, proc->priority, proc->cycles);
              //break;
              return(proc);
            }
          }

        default:
            printf("Unknown scheduling policy\n");
            exit(-1);
    }

    //return(proc);
}

void remove_process_from_execution(int cpu, int core, int hthread){

    struct pcb *proc;
    proc = computing_engine.cpus[cpu].cores[core].hthreads[hthread].proc;
    //printf("Geratzen zaizkio %ld ziklo %d PID ko prozesuari\n", proc->cycles, proc->pid);
    if(proc->pid > 0 && proc->cycles == 0){
        //printf("EZABATU ALLPROCS ILARATIK\n" );
        remove_process_allprocs_queue(proc);
    }
    else if(proc->pid == 0){
        insert_process_ready_queue(proc);
    }


}

void context_switch(int cpu, int core, int hthread, struct pcb *proc){
  //printf("Ezabatu exekuziotik...\n");
  remove_process_from_execution(cpu, core, hthread);
  //printf("hthreadera esleitu...\n");
	assign_process_to_hthread(cpu, core, hthread, proc);
  //printf("ezabatu prestilaratik,\n");
	remove_process_ready_queue(proc);
}

void dispatcher(int cpu, int core, int hthread, struct pcb *proc){
    context_switch(cpu, core, hthread, proc);
}


void schedule(int cpu, int core, int hthread){

    struct pcb *proc;

    if(process_to_be_scheduled()){
        printf("Prozesuak daude antolatzeko. Schedulerrari deitu:\n" );
        proc = scheduler();
        printf("Schedulerrak itzulitako pid: %d \n", proc->pid);
        dispatcher(cpu, core, hthread, proc);
        printf("SCHEDULE: Dispatcherra zuzen gauzatu da.\n");
        printf("\n");
    }

}


void create_ready_queue(){

    list_initialize(&ready_queue);

}

void create_prior_list(){
  for (int i=0; i<100; i++){
    list_initialize(&prior_list[i]);
  }
}

void insert_process_ready_queue(struct pcb* proc){
  if (proc->cycles>0){
    switch (conf.scheduling_policy) {
      case FIFO:
        //proc->quantum=1;
        list_append(&ready_queue, proc);
      case PRIORITY:
        list_append(&prior_list[proc->priority], proc); //Sartuko dugu prozesuren lehentasuneko posizioan, bertako prestilaran, gure prozesua azken posizioan.
        printf("INSERT: pid:%d  prior:%d  tick:%ld \n", proc->pid, proc->priority, proc->cycles );
    }
}
}

void remove_process_ready_queue(struct pcb* proc){

    switch(conf.scheduling_policy){
      case FIFO:
        list_rem_head(&ready_queue);
      case PRIORITY:
        list_rem_head(&prior_list[proc->priority]);
    }
}

long process_to_be_scheduled(){
  printf("Begiratu ea prozesurik dagoen pendiente...\n" );
  switch (conf.scheduling_policy) {
      case FIFO:
         return(!list_empty(&ready_queue));
      case PRIORITY:
        for (int i=99; i>-1; i--){
          //printf("Begiratu ea PRIORITYn prozesurik dagoen pendiente %d lehentasunean...\n", i );
          if(!list_empty(&prior_list[i])){
            return 1;
          }
        }
        printf("Ez dago prozesurik pendiente. \n");
        return 0;
}
}
